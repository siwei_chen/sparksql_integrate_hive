# sparksql 整合hive

#### 项目介绍
Spark SQL 主要目的是使得用户可以在Spark 上使用SQL，其数据
源既可以是RDD，也可以是外部的数据源（比如文本、Hive、Json 等）。
Spark SQL 的其中一个分支就是Spark on Hive，也就是使用Hive 中HQL
的解析、逻辑执行计划翻译、执行计划优化等逻辑，可以近似认为仅
将物理执行计划从MR 作业替换成了Spark 作业。SparkSql 整合hive
就是获取hive 表中的元数据信息，然后通过SparkSql 来操作数据。

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)